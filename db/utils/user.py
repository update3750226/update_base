from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from db.models.user import User
from validation.user import UserValidation


async def get_users(session: AsyncSession):
    return (await session.execute(select(User))).scalars().all()


async def create_user(user: UserValidation, session: AsyncSession):
    new_user = User(name=user.name, surname=user.surname)
    session.add(new_user)
    await session.commit()
    return new_user
