from db.db_setup import Base
from sqlalchemy import Integer, String, Column


class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    surname = Column(String)
