from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routes.user import user_router
from db.db_setup import init_db
from uvicorn import run

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)
app.include_router(user_router, prefix='/user', tags=['user'])


@app.on_event('startup')
async def on_startup():
    await init_db()


if __name__ == '__main__':
    run('main:app', reload=True)
