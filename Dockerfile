FROM python:3.10-slim

WORKDIR /app

COPY . /app
COPY .env_prod .env

RUN pip install -r requirements.txt
