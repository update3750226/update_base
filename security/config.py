from dotenv import load_dotenv
import os

load_dotenv('./.env')


class Config:
    PSQL_URL = os.environ.get('PSQL_URL')
