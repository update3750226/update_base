from fastapi import APIRouter, Depends
from db.db_setup import get_session
from sqlalchemy.ext.asyncio import AsyncSession
from db.utils.user import get_users, create_user
from validation.user import UserValidation

user_router = APIRouter()


@user_router.get('/get-users')
async def get_users_path(session: AsyncSession = Depends(get_session)):
    return await get_users(session)


@user_router.post('/create-user')
async def create_user_path(user: UserValidation, session: AsyncSession = Depends(get_session)):
    return await create_user(user, session)
