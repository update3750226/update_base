from pydantic import BaseModel


class UserValidation(BaseModel):
    name: str
    surname: str
